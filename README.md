# Learning git on Fevlake Devops

This project created for learning git

## Getting Started
 
These instructions will get you a copy of the project up and running on your local machine for learning purposes. 

### Prerequisites

For use project need installed on you system nginx and git.
On Fedora femaly linux run followed commands.

```
yum install nginx git
```

### Installing

For installation on Fedora femaly linux run command

```
git clone https://bitbucket.org/nevgin/rebrain-devops-task-checkout.git
sudo cp rebrain-devops-task-checkout/nginx.conf /etc/nginx/nginx.conf
```

## Running the tests

For check configuration, run command:
```
nginx -t
```
## Built With

* [Nginx web server](http://www.nginx.org) - The Nginx web server

## Authors

* **Aleksey Nevgin** - [Nevgin](https://github.com/nevgin)


